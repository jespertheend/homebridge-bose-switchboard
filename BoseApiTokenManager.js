const {v4: uuidv4} = require("uuid");
const fs = require("fs");

class BoseApiTokenManager{
	constructor(api, tokensFilePath){
		this.api = api;
		this.tokensFilePath = tokensFilePath;

		this.refreshTokens = new Map();

		this.loadRefreshTokens();
	}

	loadRefreshTokens(){
		if(!fs.existsSync(this.tokensFilePath)) return;
		let tokensJson = fs.readFileSync(this.tokensFilePath);
		let tokensParsed = JSON.parse(tokensJson);
		for(const token of tokensParsed){
			this.refreshTokens.set(token.uuid, {refresh: token.refreshToken});
		}
	}

	saveRefreshTokens(){
		let jsonData = [];
		for(const [uuid, tokenData] of this.refreshTokens){
			jsonData.push({uuid, refreshToken: tokenData.refresh});
		}
		let newFileContent = JSON.stringify(jsonData);
		fs.writeFileSync(this.tokensFilePath, newFileContent);
	}

	addToken(response){
		let uuid = uuidv4();
		let tokenData = {refresh};
		this.updateAccessToken(tokenData, response);
		this.refreshTokens.set(uuid, tokenData);
		this.saveRefreshTokens();
	}

	getAllAccessTokenUuids(){
		return this.refreshTokens.keys();
	}

	async getAccessTokenForUuid(uuid){
		let tokenData = this.refreshTokens.get(uuid);
		return await this.getAccessTokenForTokenData(tokenData);
	}

	async getAccessTokenForTokenData(tokenData){
		if(!tokenData) return null;
		if(!tokenData.access || tokenData.expireTime < Date.now()){
			let success = await this.refreshAccessToken(tokenData);
			if(!success) return null;
		}
		return tokenData.access;
	}

	async refreshAccessToken(tokenData){
		let response = await this.api.getAccessTokenFromRefresh(tokenData.refresh);
		if(!response) return false;
		this.updateAccessToken(tokenData, response);
		return true;
	}

	updateAccessToken(tokenData, response){
		tokenData.access = response.access_token;
		tokenData.expireTime = Date.now() + response.expires_in * 1000;
	}
}

module.exports = BoseApiTokenManager;
