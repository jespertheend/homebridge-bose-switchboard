## Installation
1. Install homebridge using: `npm install -g homebridge`
2. Install this plugin using: `npm install -g homebridge-bose-switchboard`
3. Add the platform to your configuration file.
```json
{
	"platforms": [
		{
			"platform": "BoseSwitchboard"
		}
	]
}
```
4. Then open http://[the ip of homebridge]:8000 in your browser and follow the instructions. They will guide you through setting up a bose connection.

## Configuration
|     Key           | Required  |                              Description                             |
|-------------------|-----------|----------------------------------------------------------------------|
| platform          | required  | Must be `BoseSwitchboard`                                            |
| clientId          | optional* | The client id from the connection created on developer.bose.com.     |
| clientSecret      | optional* | The client secret from the connection created on developer.bose.com. |
| port              | optional  | The port that the web portal binds to. (Default is 8000)             |
| exposeVolumeLight | optional  | Set this to true to create a Light accessory to control the volume.  |

<sub>
*This is required for the application to function but can also be added later through the web portal.
</sub>

## How it works
This plugin creates an accessory for every devices that is linked to your bose account. The accessory shows up as television, this is currently the only way to support input selection and turning the device on or off.

The input selection lists the available presets in your Bose Music app. The Switchboard api doesn't have support for getting the currently selected preset, so this plugin tries to make a best guess and shows that as the currently selected input.

There's also no way for the api to switch to Bluetooth, AirPlay or TV input source. But these inputs have been listed anyway. When you select them it turns your device off and on again. This switches the input to the most recent state except for AirPlay it seems. So this could be useful to switch the input of a SoundBar from AirPlay to TV at least.

## Roadmap
- [x] controlling devices power state
- [x] support controlling devices input source and presets
- [x] support renaming devices and input sources
- [x] setting clientId and clientSecret config from the web portal
- [x] support for controlling the volume of a device
- [ ] setting for playing sound when turning device on or off
