class SpeakerLightAccessory{
	constructor(productAccessory, platform){
		this.name = productAccessory.name+" Volume";
		this.alias = productAccessory.alias+" Volume";
		this.platform = platform;

		this.services = [];

		this.serviceType = this.platform.Service.Fan;
		this.characteristicType = this.platform.Characteristic.RotationSpeed;

		this.infoService = new this.platform.Service.AccessoryInformation();
		this.infoService.setCharacteristic(this.platform.Characteristic.Manufacturer, "Bose");
		this.infoService.setCharacteristic(this.platform.Characteristic.SerialNumber, this.productID);
		this.infoService.setCharacteristic(this.platform.Characteristic.Model, "Speaker");
		this.infoService.setCharacteristic(this.platform.Characteristic.FirmwareRevision, this.firmwareRevision);
		this.services.push(this.infoService);

		this.lightService = new this.serviceType(this.name, this.alias);
		this.lightService.getCharacteristic(this.characteristicType)
			.on("get", async cb => {
				let volume = await productAccessory.getVolume100();
				cb(volume == null ? "error getting volume" : null, volume);
			})
			.on("set", async (value, cb) => {
				let success = await productAccessory.setVolume100(value, false);
				cb(success ? null : "error while setting volume");
			});
		this.services.push(this.lightService);
	}

	getServices(){
		return this.services;
	}

	updateVolume(volume){
		this.lightService.setCharacteristic(this.characteristicType, volume);
	}

	updatePowerState(poweredOn){
		this.lightService.setCharacteristic(this.platform.Characteristic.On, poweredOn);
	}
}

module.exports = SpeakerLightAccessory;
