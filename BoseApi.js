const https = require("https");
const fs = require("fs");
const path = require("path");
const querystring = require('querystring');
const BoseApiTokenManager = require("./BoseApiTokenManager.js");

class BoseApi{
	constructor(persistPath){
		let tokensFilePath = path.resolve(persistPath, "boseSwitchboardTokens.json");
		this.tokenManager = new BoseApiTokenManager(this, tokensFilePath);
	}

	setClientData(clientId, clientSecret){
		this.clientId = clientId;
		this.clientSecret = clientSecret;
	}

	getClientData(){
		return {
			id: this.clientId,
			secret: this.clientSecret,
		}
	}

	async addToken(code, redirect_uri){
		let response = await this.getAccessTokenFromCode(code, redirect_uri);
		if(!response) return false;
		this.tokenManager.addToken(response);
		return true;
	}

	async getAccessTokenFromCode(code, redirect_uri){
		return await this.getAccessToken({
			grant_type: "authorization_code",
			code, redirect_uri,
		});
	}

	async getAccessTokenFromRefresh(refresh_token){
		return await this.getAccessToken({
			grant_type: "refresh_token",
			refresh_token,
		});
	}

	async getAccessToken(bodyData){
		let auth = "Basic "+this.base64Encode(this.clientId+":"+this.clientSecret);
		return await this.apiCall("/auth/oauth/token", {
			auth,
			method: "POST",
			bodyTypeJson: false,
			bodyData,
		});
	}

	base64Encode(data){
		let buff = new Buffer(data);
		return buff.toString('base64');
	}

	async apiCall(endpoint, {
		endPointExact=false,
		auth="",
		accessToken="",
		method="GET",
		bodyTypeJson=true,
		bodyData={},
	} = {}){
		let url = endPointExact ? endpoint : "https://partners.api.bose.io"+endpoint;
		if(accessToken){
			auth = "Bearer "+accessToken;
		}

		console.log(method,"call to",url);

		let headers = {
			"X-API-Version": 0.8,
			"X-ApiKey": this.clientId,
			"Authorization": auth,
		}
		let postData = null;
		if(method == "POST"){
			headers["Content-Type"] = bodyTypeJson ? "application/json" : "application/x-www-form-urlencoded";

			if(bodyTypeJson){
				postData = JSON.stringify(bodyData);
			}else{
				postData = querystring.stringify(bodyData);
			}
		}

		try{
			let [response, statusCode] = await new Promise((resolve, reject) => {
				let req = https.request(url, {method, headers}, res => {
					let body = "";
					res.on("data", chunk => {
						body += chunk;
					});
					res.on("end", _ => {
						try{
							body = JSON.parse(body);
						}catch(_){
							body = null;
						}
						resolve([body, res.statusCode]);
					});
				});
				req.on("error", e => {
					reject(e);
				});
				if(postData){
					req.write(postData);
				}
				req.end();
			});
			if(statusCode < 200 || statusCode >= 300){
				console.log("bose switchboard api returned an error: ", response);
				return null;
			}
			return response;
		}catch(e){
			console.log("An error occurred when connecting to the bose switchboard api servers", e);
			return null;
		}
	}

	async *getAllProducts(){
		for(const accessTokenUuid of this.tokenManager.getAllAccessTokenUuids()){
			let accessToken = await this.tokenManager.getAccessTokenForUuid(accessTokenUuid);
			for await(const product of this.getProducts(accessToken)){
				yield [accessTokenUuid, product];
			}
		}
	}

	async *getProducts(accessToken){
		let nextUrl = "/products";
		let endPointExact = false;
		while(nextUrl){
			let response = await this.apiCall(nextUrl, {accessToken, endPointExact});
			if(!response) return;
			nextUrl = response.nextPageUrl;
			endPointExact = false;
			for(const product of response.results){
				yield product;
			}
		}
	}

	async getProduct(accessToken, productID){
		return await this.apiCall(`/products/${productID}`, {accessToken});
	}

	async setPowerState(accessToken, productID, powerState){
		let response = await this.apiCall(`/products/${productID}/settings/power`, {
			method: "POST",
			accessToken,
			bodyData: {
				power: powerState,
			},
		});
		let success = !!response;
		return success;
	}

	async getPresets(accessToken, productID){
		return await this.apiCall(`/products/${productID}/presets`, {accessToken});
	}

	async getContent(accessToken, productID){
		return await this.apiCall(`/products/${productID}/content`, {accessToken});
	}

	async setPreset(accessToken, productID, presetID){
		let response = await this.apiCall(`/products/${productID}/presets/${presetID}/play`, {
			method: "POST",
			accessToken,
		});
		let success = !!response;
		return success;
	}

	async setVolume(accessToken, productID, volume){
		volume = Math.round(volume);
		let response = await this.apiCall(`/products/${productID}/settings/volume`, {
			method: "POST",
			accessToken,
			bodyData: {volume},
		});
		let success = !!response;
		return success;
	}
}

module.exports = BoseApi;
