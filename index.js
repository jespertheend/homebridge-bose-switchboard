let Service, Characteristic;
let HelperServer = require("./HelperServer.js");
let BoseApi = require("./BoseApi.js");
let BoseProductAccessory = require("./BoseProductAccessory.js");
const fs = require("fs");

let configPath = null;
let persistPath = null;

const PLUGIN_NAME = "bose-switchboard";
const PLATFORM_NAME = "BoseSwitchboard";

module.exports = function(homebridge){
	Service = homebridge.hap.Service;
	Characteristic = homebridge.hap.Characteristic;

	configPath = homebridge.user.configPath();
	persistPath = homebridge.user.persistPath();

	homebridge.registerPlatform(PLUGIN_NAME, PLATFORM_NAME, BoseSwitchboardPlatform);
}

class BoseSwitchboardPlatform{
	constructor(log, config){
		this.helperServer = new HelperServer(config.port, this);
		this.boseApi = new BoseApi(persistPath);
		this.boseApi.setClientData(config.clientId, config.clientSecret);
		this.Service = Service;
		this.Characteristic = Characteristic;
		this.exposeVolumeLight = config.exposeVolumeLight;

		this._accessories = new Map();
		this.isUpdatingAccessoriesList = false;
		this.updateAccessoriesList();
		this.onAccessoriesListUpdatedCbs = [];
	}

	getServices(){
		return [];
	}

	async accessories(cb){
		await this.waitForAccessoriesListUpdate();
		let accessories = [];
		for(const accessory of this._accessories.values()){
			await accessory.waitForServicesAdded();
			accessories.push(accessory);
			if(accessory.lightSpeaker){
				accessories.push(accessory.lightSpeaker);
			}
		}
		cb(accessories);
	}

	async updateAccessoriesList(){
		this.isUpdatingAccessoriesList = true;
		for await(const [accessTokenUuid, product] of this.boseApi.getAllProducts()){
			if(!this._accessories.has(product.productID)){
				let accessory = new BoseProductAccessory(accessTokenUuid, product, this);
				this._accessories.set(product.productID, accessory);
			}
		}
		this.isUpdatingAccessoriesList = false;
		for(const cb of this.onAccessoriesListUpdatedCbs){
			cb();
		}
		this.onAccessoriesListUpdatedCbs = [];
	}

	async waitForAccessoriesListUpdate(){
		if(!this.isUpdatingAccessoriesList) return;
		await new Promise(r => this.onAccessoriesListUpdatedCbs.push(r));
	}

	updateClientSettings(clientId, clientSecret){
		console.log("updating config for BoseSwitchboard platform");
		let configStr = fs.readFileSync(configPath);
		let config = JSON.parse(configStr);
		let platform = config.platforms.find(p => p.platform == PLATFORM_NAME);
		if(!platform) return false;
		platform.clientId = clientId;
		platform.clientSecret = clientSecret;
		configStr = JSON.stringify(config, null, 4);
		fs.writeFileSync(configPath, configStr);
		this.boseApi.setClientData(clientId, clientSecret);
		return true;
	}
}
