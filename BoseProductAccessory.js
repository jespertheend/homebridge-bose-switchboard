const SpeakerLightAccessory = require("./SpeakerLightAccessory.js");

class BoseProductAccessory{
	constructor(accessTokenUuid, productData, platform){
		this.accessTokenUuid = accessTokenUuid;
		this.productID = productData.productID;
		this.name = productData.productName;
		this.alias = productData.productName;

		this.platform = platform;
		this.services = [];

		this.didUpdateDevice = false;
		this.isUpdatingDevice = false;
		this.onDeviceUpdateCbs = [];
		this.lastDeviceUpdateTime = 0;
		this.deviceServicesAdded = false;
		this.onDeviceServicesAddedCbs = [];

		this.isUpdatingPresets = false;
		this.onPresetsUpdateCbs = [];
		this.presets = [];
		this.presetServicesAdded = false;
		this.onPresetServicesAddedCbs = [];

		this.inputs = [];

		this.supportedPowerSettings = [];
		this.currentPowerStateOn = false;
		this.firmwareRevision = "1337";

		this._currentVolume = 0;
		this.volumeMin = 0;
		this.volumeMax = 100;

		if(platform.exposeVolumeLight){
			this.lightSpeaker = new SpeakerLightAccessory(this, platform);
		}else{
			this.lightSpeaker = null;
		}

		this.infoService = null;

		this.tvService = new this.platform.Service.Television(this.name);
		this.tvService.getCharacteristic(this.platform.Characteristic.Active)
			.on("get", async cb => {
				let powerState = await this.getPowerState();
				cb(powerState == null ? "error getting power state" : null, powerState);
			})
			.on("set", async (value, cb) => {
				let success = await this.setPowerState(value);
				if(success){
					cb(null);
				}else{
					cb("error while setting power state");
				}
			});
		this.tvService.getCharacteristic(this.platform.Characteristic.ActiveIdentifier)
			.on("get", async cb => {
				let activeInput = await this.getActiveInput();
				cb(activeInput == null ? "error while getting active input" : null, activeInput);
			})
			.on("set", async (value, cb) => {
				let success = await this.setActiveInput(value);
				if(success){
					cb(null);
				}else{
					cb("error while setting active input");
				}
			});
		this.services.push(this.tvService);

		this.tvSpeakerService = new this.platform.Service.TelevisionSpeaker("TV Speaker","TV Speaker");
		this.tvSpeakerService.setCharacteristic(this.platform.Characteristic.Active, this.platform.Characteristic.Active.ACTIVE);
		this.tvSpeakerService.setCharacteristic(this.platform.Characteristic.VolumeControlType, this.platform.Characteristic.VolumeControlType.ABSOLUTE);
		this.tvSpeakerService.getCharacteristic(this.platform.Characteristic.Volume)
			.on("get", async cb => {
				let volume = await this.getVolume();
				cb(volume == null ? "error getting volume" : null, volume);
			})
			.on("set", async (value, cb) => {
				let success = await this.setVolume(value);
				cb(success ? null : "error while setting volume");
			});
		this.tvSpeakerService.getCharacteristic(this.platform.Characteristic.VolumeSelector)
			.on("get", async cb => {
				cb(null, 0);
			})
			.on("set", async (value, cb) => {
				let success = await this.offsetVolume(value ? -3 : 3);
				cb(success ? null : "error while setting volume selector");
			});
		this.services.push(this.tvSpeakerService);

		this.updateDevice();
		this.addDeviceServices();
		this.addPresetServices();
	}

	getServices(){
		return this.services;
	}

	async getAccessToken(){
		return await this.platform.boseApi.tokenManager.getAccessTokenForUuid(this.accessTokenUuid);
	}

	async addDeviceServices(){
		await this.waitForDeviceUpdate();
		this.infoService = new this.platform.Service.AccessoryInformation();
		this.infoService.setCharacteristic(this.platform.Characteristic.Manufacturer, "Bose");
		this.infoService.setCharacteristic(this.platform.Characteristic.SerialNumber, this.productID);
		this.infoService.setCharacteristic(this.platform.Characteristic.Model, "Speaker");
		this.infoService.setCharacteristic(this.platform.Characteristic.FirmwareRevision, this.firmwareRevision);
		this.services.push(this.infoService);
		this.deviceServicesAdded = true;
		for(const cb of this.onDeviceServicesAddedCbs){
			cb();
		}
		this.onDeviceServicesAddedCbs = [];
	}

	async addPresetServices(){
		await this.updatePresets();

		const InputSourceType = this.platform.Characteristic.InputSourceType;
		this.addInput("Unknown", "unknown", InputSourceType.OTHER);
		this.addInput("TV", "Device", InputSourceType.TUNER, async _ => await this.restart());
		this.addInput("AirPlay", "AirPlay", InputSourceType.AIRPLAY, async _ => await this.restart());
		this.addInput("Bluetooth", "Bluetooth", InputSourceType.OTHER, async _ => await this.restart());
		for(const [id, preset] of Object.entries(this.presets)){
			if(!preset) continue;
			this.addInput(preset.name, preset.source, InputSourceType.APPLICATION, async _ => {
				return await this.platform.boseApi.setPreset(await this.getAccessToken(), this.productID, preset.presetID);
			});
		}

		this.presetServicesAdded = true;
		for(const cb of this.onPresetServicesAddedCbs){
			cb();
		}
		this.onPresetServicesAddedCbs = [];
	}

	addInput(displayName, sourceDisplayName, sourceType, onSetCb=null){
		let identifier = this.inputs.length;
		let inputService = new this.platform.Service.InputSource(displayName, displayName);
		inputService.setCharacteristic(this.platform.Characteristic.Identifier, identifier);
		inputService.setCharacteristic(this.platform.Characteristic.IsConfigured, this.platform.Characteristic.IsConfigured.CONFIGURED);
		inputService.setCharacteristic(this.platform.Characteristic.InputSourceType, sourceType);
		this.services.push(inputService);
		this.tvService.addLinkedService(inputService);
		this.inputs.push({identifier, sourceDisplayName, inputService, onSetCb});
	}

	async getActiveInput(){
		let response = await this.platform.boseApi.getContent(await this.getAccessToken(), this.productID);
		if(!response) return null;
		let sourceDisplayName = response.source.sourceDisplayName;
		for(const input of this.inputs){
			if(input.sourceDisplayName == sourceDisplayName) return input.identifier;
		}
		return 0;
	}

	async setActiveInput(inputId){
		for(const input of this.inputs){
			if(input.identifier == inputId){
				if(input.onSetCb) return await input.onSetCb();
			}
		}
		return false;
	}

	async getPowerState(){
		let success = await this.updateDevice();
		if(!success) return null;
		return this.currentPowerStateOn;
	}

	async restart(){
		let success = await this.setPowerState(false);
		if(!success) return false;
		let success2 = await this.setPowerState(true);
		if(!success2) return false;
		return true;
	}

	async setPowerState(on){
		let powerStateStr = await this.boolToPowerState(on);
		if(powerStateStr == null) return false;
		let success = await this.platform.boseApi.setPowerState(await this.getAccessToken(), this.productID, powerStateStr);
		if(!success) return false;
		if(this.lightSpeaker) this.lightSpeaker.updatePowerState(on);
		return true;
	}

	powerStateToBool(powerState){
		return powerState == "ON";
	}

	async boolToPowerState(on){
		if(!this.didUpdateDevice){
			let success = await this.updateDevice();
			if(!success) return null;
		}
		if(this.supportedPowerSettings.length <= 0) return on ? "ON" : "STANDBY";
		if(this.supportedPowerSettings.length == 1) return this.supportedPowerSettings[0];
		if(on){
			if(this.supportedPowerSettings.includes("ON")) return "ON";
			return this.supportedPowerSettings[0];
		}else{
			let withoutOn = this.supportedPowerSettings.filter(s => s != "ON");
			if(withoutOn.length > 0){
				return withoutOn[0];
			}else{
				return this.supportedPowerSettings[0];
			}
		}
	}

	async updateDevice(){
		if(this.isUpdatingDevice){
			return await this.waitForDeviceUpdate();
		}
		this.isUpdatingDevice = true;
		let product = await this.platform.boseApi.getProduct(await this.getAccessToken(), this.productID);
		let success = !!product;
		if(success){
			this.didUpdateDevice = true;
			this.lastDeviceUpdateTime = Date.now();
			this.supportedPowerSettings = product.capabilities.supportedPowerSettings;
			this.currentPowerStateOn = this.powerStateToBool(product.settings.power);
			if(this.lightSpeaker) this.lightSpeaker.updatePowerState(this.currentPowerStateOn);
			this.firmwareRevision = product.settings.firmwareVersion;
		}
		this.isUpdatingDevice = false;
		for(const cb of this.onDeviceUpdateCbs){
			cb(success);
		}
		this.onDeviceUpdateCbs = [];
		return success;
	}

	async waitForDeviceUpdate(){
		if(!this.isUpdatingDevice) return;
		return await new Promise(r => this.onDeviceUpdateCbs.push(r));
	}

	async updatePresets(){
		if(this.isUpdatingPresets){
			return await this.waitForPresetsUpdate();
		}
		this.isUpdatingPresets = true;

		this.presets = await this.platform.boseApi.getPresets(await this.getAccessToken(), this.productID);
		if(!this.presets) this.presets = [];

		this.isUpdatingPresets = false;
		for(const cb of this.onPresetsUpdateCbs){
			cb();
		}
		this.onPresetsUpdateCbs = [];
	}

	async waitForPresetsUpdate(){
		if(!this.isUpdatingPresets) return;
		return await new Promise(r => this.onPresetsUpdateCbs.push(r));
	}

	async waitForDeviceServicesAdded(){
		if(this.deviceServicesAdded) return;
		return await new Promise(r => this.onDeviceServicesAddedCbs.push(r));
	}

	async waitForPresetServicesAdded(){
		if(this.presetServicesAdded) return;
		return await new Promise(r => this.onPresetServicesAddedCbs.push(r));
	}

	async waitForServicesAdded(){
		await this.waitForDeviceServicesAdded();
		await this.waitForPresetServicesAdded();
	}

	async getVolume(maxCacheTime=3000){
		if(Date.now() - this.lastDeviceUpdateTime < maxCacheTime){
			return this._currentVolume;
		}
		let product = await this.platform.boseApi.getProduct(await this.getAccessToken(), this.productID);
		if(!product) return null;
		this._currentVolume = product.settings.volume;
		this.volumeMin = product.capabilities.volumeMin;
		this.volumeMax = product.capabilities.volumeMax;
		this.updateLastDeviceUpdateTime();
		return this._currentVolume;
	}

	async setVolume(newValue, updateLightSpeaker=true){
		if(newValue == this._currentVolume && Date.now() - this.lastDeviceUpdateTime < 2000) return true;
		newValue = Math.max(this.volumeMin, Math.min(this.volumeMax, newValue));
		let success = await this.platform.boseApi.setVolume(await this.getAccessToken(), this.productID, newValue);
		if(success){
			this._currentVolume = newValue;
			this.updateLastDeviceUpdateTime();
			if(this.lightSpeaker && updateLightSpeaker){
				this.lightSpeaker.updateVolume(this.volumeToVolume100(newValue));
			}
		}
		return success;
	}

	lerp(a,b,t){
		return a + t * (b-a);
	}

	//inverse lerp
	iLerp(a,b,t){
		return (t - a) / (b - a);
	}

	clamp(v, min, max){
		return Math.max(min, Math.min(max, v));
	}

	clamp01(v){
		return this.clamp(v, 0, 1);
	}

	mapValue(fromMin,fromMax,toMin,toMax,val,performClamp){
		let lerpedVal = this.iLerp(fromMin,fromMax,val);
		if(performClamp) lerpedVal = this.clamp01(lerpedVal);
		return this.lerp(toMin,toMax,lerpedVal);
	}

	volumeToVolume100(volume){
		return this.mapValue(this.volumeMin, this.volumeMax, 0, 100, volume, true);
	}

	async getVolume100(maxCacheTime=3000){
		let volume = await this.getVolume(maxCacheTime);
		if(volume == null) return null;
		return this.volumeToVolume100(volume);
	}

	async setVolume100(volume, updateLightSpeaker=true){
		volume = this.mapValue(0, 100, this.volumeMin, this.volumeMax, volume, true);
		return await this.setVolume(volume, updateLightSpeaker);
	}

	updateLastDeviceUpdateTime(){
		this.lastDeviceUpdateTime = Date.now();
	}

	async offsetVolume(offset=0, updateLightSpeaker=true){
		if(offset == 0) return;
		let volume = await this.getVolume();
		if(volume == null) return false;
		volume += offset;
		return await this.setVolume(volume, updateLightSpeaker);
	}
}

module.exports = BoseProductAccessory;
