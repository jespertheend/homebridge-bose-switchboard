const http = require("http");
const fs = require("fs");
const path = require("path");
const querystring = require('querystring');
const URL = require("url");

class HelperServer{
	constructor(port, platform){
		port = port || 8000;
		this.platform = platform;
		console.log("starting bose-switchboard helper server on port "+port);
		http.createServer((request, response) => {
			this.handleRequest(request, response);
		}).listen(port);

		this.instructionsHtml = fs.readFileSync(path.resolve(__dirname, "instructions.html"));
	}

	async handleRequest(request, response){
		if(request.method == "GET" && request.url == "/"){
			response.writeHead(200, {'Content-type':'text/html'});
			response.write(this.instructionsHtml);
			response.end();
		}else if(request.method == "GET" && request.url.startsWith("/updateClientData?")){
			let url = URL.parse(request.url);
			let queryData = querystring.decode(url.query);
			let clientId = queryData.clientId;
			let clientSecret = queryData.clientSecret;
			let success = this.platform.updateClientSettings(clientId, clientSecret);
			if(success){
				response.writeHead(200, {'Content-type':'text/html'});
			}else{
				response.writeHead(500, {'Content-type':'text/html'});
			}
			response.end();
		}else if(request.method == "GET" && request.url == "/getClientData"){
			response.writeHead(200, {'Content-type':'application/json'});
			response.write(JSON.stringify(this.platform.boseApi.getClientData()));
			response.end();
		}else if(request.method == "GET" && request.url.startsWith("/authResponse?")){
			let url = URL.parse(request.url);
			let queryData = querystring.decode(url.query);
			let code = queryData.code;
			let state = queryData.state;
			let success = false;
			if(code && state){
				let redirectUri = null;
				try{
					let parsedJson = JSON.parse(state);
					redirectUri = parsedJson.redirectUri;
				}catch(e){}
				if(redirectUri){
					success = await this.platform.boseApi.addToken(code, redirectUri);
				}
			}
			if(success){
				response.writeHead(200, {'Content-type':'text/plain'});
				response.write("Your application has been authorized!");
			}else{
				response.writeHead(502, {'Content-type':'text/plain'});
				response.write("Whoops, something went wrong while authorizing this application :(");
			}
			response.end();
		}else{
			response.writeHead(404, {'Content-type':'text/plain'});
			response.end();
		}
	}
}

module.exports = HelperServer;
